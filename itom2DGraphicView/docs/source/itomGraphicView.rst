===========================================================
graphicViewPlot - itom desinger widget
===========================================================

=============== ========================================================================================================
**Summary**:    :plotsummary:`graphicViewPlot`
**Type**:       :plottype:`graphicViewPlot`
**Input**:       :plotinputtype:`graphicViewPlot`
**Formats**:       :plotdataformats:`graphicViewPlot`
**Features**:       :plotfeatures:`graphicViewPlot`
**License**:    :plotlicense:`graphicViewPlot`
**Platforms**:  Windows, Linux
**Author**:     :plotauthor:`graphicViewPlot`
=============== ========================================================================================================

