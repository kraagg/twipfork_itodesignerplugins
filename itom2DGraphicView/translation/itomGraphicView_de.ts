<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>Dialog2DScale</name>
    <message>
        <location filename="../dialog2DScale.ui" line="+14"/>
        <source>Plot Interval Settings</source>
        <translation>Skaleneinstellungen</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>x-range</source>
        <translation>X-Anzeige</translation>
    </message>
    <message>
        <location line="+12"/>
        <location line="+108"/>
        <location line="+105"/>
        <source>auto calc once</source>
        <translation>Automatisch</translation>
    </message>
    <message>
        <location line="-206"/>
        <location line="+108"/>
        <location line="+105"/>
        <source>manual adjustment:</source>
        <translation>Manuelle Ausrichtung:</translation>
    </message>
    <message>
        <location line="-188"/>
        <location line="+108"/>
        <location line="+105"/>
        <location line="+79"/>
        <source>from</source>
        <translation>von</translation>
    </message>
    <message>
        <location line="-256"/>
        <location line="+108"/>
        <location line="+111"/>
        <source>to</source>
        <translation>bis</translation>
    </message>
    <message>
        <location line="-191"/>
        <source>y-range</source>
        <translation>Y-Anzeige</translation>
    </message>
    <message>
        <location line="+105"/>
        <source>z-range</source>
        <translation>Z-Anzeige</translation>
    </message>
    <message>
        <location line="+114"/>
        <source>current plane</source>
        <translation>Aktuelle Ebene</translation>
    </message>
</context>
<context>
    <name>GraphicViewPlot</name>
    <message>
        <location filename="../graphicViewPlot.cpp" line="+63"/>
        <source>Points for line plots from 2D objects</source>
        <translation type="unfinished">Punkte für Linienplot von 2D-Objekten</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>plotting tools</source>
        <translation type="unfinished">Symbolleiste Plot</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>-     NOIMAGE     -</source>
        <translation type="unfinished">-     KEIN BILD     -</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>View</source>
        <translation type="unfinished">Ansicht</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Tools</source>
        <translation type="unfinished">Werkzeuge</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>plot2D</source>
        <translation></translation>
    </message>
    <message>
        <location line="+25"/>
        <source>deprecation warning: starting with itom version 3.3 this plot will be removed.</source>
        <translation type="unfinished">Vorwarnung: Dieser Plot steht ab der itom Version 3.3 nicht mehr zur Verfügung.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>home</source>
        <translation type="unfinished">Ansicht zurücksetzen</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Reset original view</source>
        <translation type="unfinished">Ursprüngliche Ansicht wiederherstellen</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>save...</source>
        <translation type="unfinished">Speichern...</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Export current view...</source>
        <translation type="unfinished">Aktuelle Abbildung exportieren...</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>scale settings...</source>
        <translation type="unfinished">Skaleneinstellungen...</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Set the ranges and offsets of this view...</source>
        <translation type="unfinished">Einstellungen der Skalen...</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>move</source>
        <translation type="unfinished">Verschieben</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Pan axes with left mouse, zoom with right</source>
        <translation type="unfinished">Achsen verschieben mit linker Maustaste, zoomen mit rechter Maustaste</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>rectangle zoom</source>
        <translation type="unfinished">Zoom in Rechteck</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Zoom to rectangle</source>
        <translation type="unfinished">Zoom in Rechteck</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>marker</source>
        <translation type="unfinished">Positionsanzeiger</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Show a point marker</source>
        <translation type="unfinished">Positionsanzeiger</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>linecut</source>
        <translation type="unfinished">Linienschnitt</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show a in plane line cut</source>
        <translation type="unfinished">Linienschnitt der Ebene anzeigen</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>color palettes</source>
        <translation type="unfinished">Farbpalette wechseln</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Switch between color palettes</source>
        <translation type="unfinished">Wechsel der Farbpaletten</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>color bar</source>
        <translation type="unfinished">Farbleiste anzeigen</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Toggle visibility of the color bar name</source>
        <translation type="unfinished">Farbleiste ein- und ausblenden</translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+1"/>
        <source>zoom level</source>
        <translation type="unfinished">Zoom-Stufen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>off</source>
        <translation type="unfinished">aus</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Switch between different zoom levels with fixed aspect ration</source>
        <translation type="unfinished">Zwischen Zoom-Stufen mit fester Bildauflösung wählen</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>slice in z-direction</source>
        <translation type="unfinished">In Z-Richtung schieben</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Show a slice through z-Stack</source>
        <translation type="unfinished">In Z-Richtung schieben</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Select image plane</source>
        <translation type="unfinished">Bildebene auswählen</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>complex switch</source>
        <translation type="unfinished">Komplex-Schalter</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Complex Switch</source>
        <translation type="unfinished">Komplex ein- und ausschlaten</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+485"/>
        <source>imaginary</source>
        <translation type="unfinished">Imaginär</translation>
    </message>
    <message>
        <location line="-484"/>
        <location line="+489"/>
        <source>real</source>
        <translation type="unfinished">Real</translation>
    </message>
    <message>
        <location line="-488"/>
        <source>absolute</source>
        <translation type="unfinished">Absolut</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+492"/>
        <source>phase</source>
        <translation type="unfinished">Phase</translation>
    </message>
    <message>
        <location line="-489"/>
        <source>Switch imaginary, real, absolute, phase</source>
        <translation type="unfinished">Komplexitätstyp</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>color switch</source>
        <translation type="unfinished">Farbwahl</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Color Switch</source>
        <translation type="unfinished">Farbwahl</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>autoColor</source>
        <translation type="unfinished">Automatische Farbe</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+511"/>
        <source>falseColor, bitshift</source>
        <translation type="unfinished">Falschfarbe, Bitverschiebung</translation>
    </message>
    <message>
        <location line="-510"/>
        <location line="+514"/>
        <source>falseColor, scaled</source>
        <translation type="unfinished">Falschfarbe, skaliert</translation>
    </message>
    <message>
        <location line="-513"/>
        <location line="+517"/>
        <source>Color, 24-Bit</source>
        <translation type="unfinished">Farbe, 24-Bit</translation>
    </message>
    <message>
        <location line="-516"/>
        <location line="+520"/>
        <source>Color, 32-Bit</source>
        <translation type="unfinished">Farbe, 32-Bit</translation>
    </message>
    <message>
        <location line="-518"/>
        <source>Switch index and direct color mode</source>
        <translation type="unfinished">Farbmodus</translation>
    </message>
    <message>
        <location line="+320"/>
        <source>PDF Documents (*.pdf)</source>
        <translation type="unfinished">FDP-Dokumente (&apos;.pdf)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>SVG Documents (*.svg)</source>
        <translation type="unfinished">SVG-Dokumente (*.svg)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Postscript Documents (*.ps)</source>
        <translation type="unfinished">Postscript-Dateien (*.ps)</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Images (</source>
        <translation type="unfinished">Bilddateien (</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Export File Name</source>
        <translation type="unfinished">Speichern unter</translation>
    </message>
    <message>
        <location line="+501"/>
        <location line="+19"/>
        <location line="+107"/>
        <location line="+11"/>
        <location line="+11"/>
        <location line="+12"/>
        <location line="+23"/>
        <location line="+11"/>
        <source>Not implemented yet.</source>
        <translation type="unfinished">Noch nicht implementiert.</translation>
    </message>
    <message>
        <location line="+49"/>
        <location line="+5"/>
        <source>Specified color type out of range [0, 4].</source>
        <translation type="unfinished">Definierter Farbtyp außerhalb des erlaubten Bereichs [0, 4].</translation>
    </message>
    <message>
        <location line="+121"/>
        <source>Export image failed, canvas handle not initilized</source>
        <translation type="unfinished">Der Bildexport ist fehlgeschlagen, Canvas-Handle wurde nicht inizialisiert</translation>
    </message>
</context>
<context>
    <name>PlotWidget</name>
    <message>
        <location filename="../plotWidget.cpp" line="+935"/>
        <source>No color maps defined.</source>
        <translation type="unfinished">Kein Farbschema definiert.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Can not toggle colorbar while using RGB-Colors.</source>
        <translation type="unfinished">Bei RGB-Farben kann die Farbleiste nicht umgeschalten werden.</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>error when loading color map</source>
        <translation type="unfinished">Fehler beim Laden des Farbschemas</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Selected color bar invalid</source>
        <translation type="unfinished">Ausgewählte Farbleiste ist ungültig</translation>
    </message>
    <message>
        <location line="+92"/>
        <location line="+6"/>
        <source>Not implemented yet.</source>
        <translation type="unfinished">Noch nicht implementiert.</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Source data for plot</source>
        <translation type="vanished">Quelldaten der Abbildung</translation>
    </message>
    <message>
        <source>Actual output data of plot</source>
        <translation type="vanished">Quelldaten der Abbildung</translation>
    </message>
    <message>
        <source>duplicate Channel, in addChannel</source>
        <translation type="obsolete">Doppelte Verbindung (in addChannel)</translation>
    </message>
    <message>
        <source>parameters incompatible, while adding channel</source>
        <translation type="obsolete">Nicht kompatible Parameter (in adding channel)</translation>
    </message>
    <message>
        <source>undefined channel direction, while adding channel</source>
        <translation type="obsolete">Undefinierte Verbindungsrichtung (in adding channel)</translation>
    </message>
    <message>
        <source>invalid child pointer, in addChannel</source>
        <translation type="obsolete">Ungültiger child pointer (in addChannel)</translation>
    </message>
    <message>
        <source>channel does not exist</source>
        <translation type="obsolete">Verbindung existiert nicht</translation>
    </message>
    <message>
        <source>Parameter: does not exist in updateParam</source>
        <translation type="obsolete">Parameter gibt es in updateParam nicht</translation>
    </message>
    <message>
        <source>Running update on a locked input channel, i.e. updatePending flag is not set</source>
        <translation type="obsolete">Fehler beim Versuch eine gelockte Verbindung zu aktualisieren. Vielleicht wurde das updatePending-Flag nicht gesetzt</translation>
    </message>
    <message>
        <source>Channel is already updating</source>
        <translation type="obsolete">Verbindung wurde bereits aktualisiert</translation>
    </message>
    <message>
        <source>parameters in list could not be found in channels, in updateChannels</source>
        <translation type="obsolete">Die Parameter der Liste wurden in &apos;channels&apos; nicht gefunden (in updateChannels)</translation>
    </message>
    <message>
        <source>channel is not a sender in setUpdatePending</source>
        <translation type="obsolete">Verbindung kann nicht als Quelle genutzt werden (in setUpdatePending)</translation>
    </message>
    <message>
        <source>unknown channel in setUpdatePending</source>
        <translation type="obsolete">Unbekannte Verbindung (in setUpdatePending)</translation>
    </message>
    <message>
        <source>Live data source for plot</source>
        <translation type="vanished">Live-Datenquelle für Abbildung</translation>
    </message>
    <message>
        <location filename="../graphicViewPlugin.cpp" line="+38"/>
        <source>ITOM widget for 2D DataObjects based on qGraphicView.</source>
        <translation type="unfinished">itom-Widget für 2D-Objekte basierend auf &apos;qGraphicView&apos;.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>LGPL</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ito::AbstractDObjFigure</name>
    <message>
        <source>Function &apos;spawnLinePlot&apos; not supported from this plot widget</source>
        <translation type="obsolete">Die Funktion &apos;spawnLinePlot&apos; wird von diesem Widget nicht unterstützt</translation>
    </message>
    <message>
        <source>Figure does not contain an input slot for live sources</source>
        <translation type="obsolete">Die Abbildung hat keine Schnittstelle für Livebilder</translation>
    </message>
</context>
<context>
    <name>ito::AbstractFigure</name>
    <message>
        <source>Properties</source>
        <translation type="obsolete">Eigenschaften</translation>
    </message>
</context>
</TS>
