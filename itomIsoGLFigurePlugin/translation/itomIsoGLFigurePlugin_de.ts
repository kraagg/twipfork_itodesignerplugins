<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>ItomIsoGLWidget</name>
    <message>
        <location filename="../itomIsoGLFigure.cpp" line="+65"/>
        <source>Points for line plots from 2d objects</source>
        <translation>Punkte für Linienplot von 2D-Objekten</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>plot2D</source>
        <translation></translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Home</source>
        <translation>Ansicht zurücksetzen</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Reset original view</source>
        <translation>Ursprüngliche Ansicht wiederherstellen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Save...</source>
        <translation>Speichern...</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Export current view...</source>
        <translation>Aktuelle Abbildung exportieren...</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Scale Settings...</source>
        <translation>Skaleneinstellungen...</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Set the ranges and offsets oif this view</source>
        <translation>Einstellungen der Skalen</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Palette</source>
        <translation>Farbpalette wechseln</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Switch between color palettes</source>
        <translation>Wechsel der Farbpaletten</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show Colorbar</source>
        <translation>Farbleiste anzeigen</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Toggle visibility of the color bar on right canvas side</source>
        <translation>Farbleiste ein- und ausblenden</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Change Background Color</source>
        <translation type="unfinished">Hintergrundfarbe ändern</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Switch between the different background colors</source>
        <translation type="unfinished">Hintergrundfarbe ändern</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Enable Illumination</source>
        <translation type="unfinished">Beleuchtung einschalten</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Enable illumination rendering</source>
        <translation type="unfinished">Beleuchtung einschalten</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+3"/>
        <source>Change illumination direction</source>
        <translation type="unfinished">Beleuchtungrichtung ändern</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Triangles</source>
        <translation type="unfinished">Dreieck</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mode Switch</source>
        <translation type="unfinished">Auswahlmodus</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>M++Mode</source>
        <translation type="unfinished">M++-Modus</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>JoeMode</source>
        <translation type="unfinished">Joe-Modus</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+3"/>
        <source>Show Infotext</source>
        <translation type="unfinished">Infotext anzeigen</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Switch Imag, Real, Abs, Pha</source>
        <translation type="unfinished">Schalter für Imag, Real, Abs, Pha</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Complex Switch</source>
        <translation type="unfinished">Komplex-Schalter</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Imag</source>
        <translation></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Real</source>
        <translation></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Abs</source>
        <translation></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Pha</source>
        <translation></translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Iso Toolbar</source>
        <translation type="unfinished">Symbolleiste ISO</translation>
    </message>
    <message>
        <location line="+260"/>
        <source>SVG Documents (*.svg)</source>
        <translation type="unfinished">SVG-Dokumente (*.svg)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Images (</source>
        <translation type="unfinished">Bilddateien (</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Export File Name</source>
        <translation type="unfinished">Speichern unter</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Source data for plot</source>
        <translation type="vanished">Quelldaten der Abbildung</translation>
    </message>
    <message>
        <source>Actual output data of plot</source>
        <translation type="obsolete">Quelldaten der Abbildung</translation>
    </message>
    <message>
        <source>duplicate Channel, in addChannel</source>
        <translation type="obsolete">Doppelte Verbindung (in addChannel)</translation>
    </message>
    <message>
        <source>parameters incompatible, while adding channel</source>
        <translation type="obsolete">Nicht kompatible Parameter (in adding channel)</translation>
    </message>
    <message>
        <source>undefined channel direction, while adding channel</source>
        <translation type="obsolete">Undefinierte Verbindungsrichtung (in adding channel)</translation>
    </message>
    <message>
        <source>invalid child pointer, in addChannel</source>
        <translation type="obsolete">Ungültiger child pointer (in addChannel)</translation>
    </message>
    <message>
        <source>channel does not exist</source>
        <translation type="obsolete">Verbindung existiert nicht</translation>
    </message>
    <message>
        <source>Parameter: does not exist in updateParam</source>
        <translation type="obsolete">Parameter gibt es in updateParam nicht</translation>
    </message>
    <message>
        <source>Running update on a locked input channel, i.e. updatePending flag is not set</source>
        <translation type="obsolete">Fehler beim Versuch eine gelockte Verbindung zu aktualisieren. Vielleicht wurde das updatePending-Flag nicht gesetzt</translation>
    </message>
    <message>
        <source>Channel is already updating</source>
        <translation type="obsolete">Verbindung wurde bereits aktualisiert</translation>
    </message>
    <message>
        <source>parameters in list could not be found in channels, in updateChannels</source>
        <translation type="obsolete">Die Parameter der Liste wurden in &apos;channels&apos; nicht gefunden (in updateChannels)</translation>
    </message>
    <message>
        <source>channel is not a sender in setUpdatePending</source>
        <translation type="obsolete">Verbindung kann nicht als Quelle genutzt werden (in setUpdatePending)</translation>
    </message>
    <message>
        <source>unknown channel in setUpdatePending</source>
        <translation type="obsolete">Unbekannte Verbindung (in setUpdatePending)</translation>
    </message>
    <message>
        <source>Live data source for plot</source>
        <translation type="vanished">Live-Datenquelle für Abbildung</translation>
    </message>
    <message>
        <location filename="../itomIsoGLFigurePlugin.cpp" line="+39"/>
        <source>ITOM widget for isometric visualisation of 2D DataObjects.</source>
        <translation>itom-Widget für isometrische Visualisierung von 2D-Objekten.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>LGPL with ITO itom-exception</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ito::AbstractDObjFigure</name>
    <message>
        <source>Function &apos;spawnLinePlot&apos; not supported from this plot widget</source>
        <translation type="obsolete">Die Funktion &apos;spawnLinePlot&apos; wird von diesem Widget nicht unterstützt</translation>
    </message>
    <message>
        <source>Figure does not contain an input slot for live sources</source>
        <translation type="obsolete">Die Abbildung hat keine Schnittstelle für Livebilder</translation>
    </message>
</context>
<context>
    <name>ito::AbstractFigure</name>
    <message>
        <source>Properties</source>
        <translation type="obsolete">Eigenschaften</translation>
    </message>
</context>
<context>
    <name>plotGLWidget</name>
    <message>
        <location filename="../plotIsoGLWidget.cpp" line="+929"/>
        <location line="+250"/>
        <location line="+15"/>
        <source>Error allocating memory</source>
        <translation type="unfinished">Fehler bei der Speicheranforderung</translation>
    </message>
    <message>
        <location line="-214"/>
        <location line="+691"/>
        <source>invalid point cloud</source>
        <translation type="unfinished">Ungültige Punktewolke</translation>
    </message>
    <message>
        <location line="-635"/>
        <location line="+399"/>
        <source>Error calculating points / triangles</source>
        <translation type="unfinished">Fehler beim Berechnen der Punkte / Dreiecke</translation>
    </message>
    <message>
        <location line="-347"/>
        <source>DataObject empty, calc triangles failed</source>
        <translation type="unfinished">Leeres DataObjects. Die Dreiecksberechnung ist fehlgeschlagen</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>Unknown DataObject-Type, calc triangles failed</source>
        <translation type="unfinished">Unbekannter Typ des DataObjects. Die Dreiecksberechnung ist fehlgeschlagen</translation>
    </message>
    <message>
        <location line="+551"/>
        <source>compiled without pointCloud support</source>
        <translation type="unfinished">Die Anwendung wurde ohne PCL (pointCloud) kompiliert</translation>
    </message>
    <message>
        <location line="+164"/>
        <source>DataObject-Container empty and compiled without pointCloud support</source>
        <translation type="unfinished">Die Anwendung wurde ohne PCL (pointCloud) kompiliert und der Datenobjekt-Container ist leer</translation>
    </message>
</context>
</TS>
